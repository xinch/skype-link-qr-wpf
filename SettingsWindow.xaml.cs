﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

using SKYPE4COMLib;

namespace skype_links_to_qr
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обработчик кнопки Ok, сохраняет параметры в файл
        /// </summary>
        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            List<string> param = new List<string>();

            param.Add("set.xml");
            param.Add(this.charsetComboBox.SelectedIndex.ToString());
            param.Add(this.errLvlComboBox.SelectedIndex.ToString());
            param.Add(this.widthTextBox.Text);
            param.Add(this.heigthTextBox.Text);
            param.Add(this.showtimeTextBox.Text);

            QrParam qr = new QrParam(param);

            this.Close();
        }

        /// <summary>
        /// Метод загружает параметры qr из файла, грузит их на форму
        /// </summary>
        private void settingsWindow_Loaded(object sender, RoutedEventArgs e)
        {
            QrParam qr = new QrParam("set.xml");
            List<string> param = qr.ParamGet();
            ComboBoxItem cb = new ComboBoxItem();
            this.charsetComboBox.SelectedIndex = Convert.ToInt32(param[0]);
            this.errLvlComboBox.SelectedIndex = Convert.ToInt32(param[1]);

            this.widthTextBox.Text = param[2];     
            this.heigthTextBox.Text = param[3];     
            this.showtimeTextBox.Text = param[4];
        }
    }

    /// <summary>
    /// класс хранит параметры qr кода
    /// </summary>
    public class QrParam
    {
        private List<string> parametrs;

        public List<string> ParamGet()
        {
            return parametrs;
        }

        /// <summary>
        /// Метод заполняет лист параметров из файла
        /// </summary>
        /// <param name="path">путь к файлу</param>
        private void LoadFromXml(string path)
        {
            parametrs = new List<string>();
            XmlDocument get = new XmlDocument();
            get.Load(path);
            foreach (XmlNode node in get.DocumentElement.ChildNodes)                             
                foreach (XmlNode param in node.ChildNodes)
                {               
                        parametrs.Add(param.Value);
    
                    foreach (XmlNode param_ch in param.ChildNodes)                      
                            parametrs.Add(param_ch.Value);
                }                   
        }

        public QrParam(List<string> _parametrs) // конструктор для сохранения
        {
            parametrs = _parametrs;
            SaveToXml();
        }

        public QrParam(string path) // конструктор для загрузки в память
        {
            LoadFromXml(path);        
        }

        /// <summary>
        /// Метод сохраняет параметры в файл
        /// </summary>
        private void SaveToXml()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true; 
            settings.IndentChars = "  ";
            settings.NewLineChars = "\n";
            settings.OmitXmlDeclaration = true;

            using (XmlWriter set = XmlWriter.Create(parametrs[0], settings)) 
            {
                int i=0;
                set.WriteStartElement("QR");
                
                set.WriteElementString("charset", parametrs[++i]);
                set.WriteElementString("errlvl", parametrs[++i]);

                set.WriteElementString("qr_width", parametrs[++i]);
                set.WriteElementString("qr_heigth", parametrs[++i]);

                set.WriteElementString("showtime", parametrs[++i]);
                set.WriteEndElement();
                set.Flush();
                set.Close();
            }
        }
    }
}
