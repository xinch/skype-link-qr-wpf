﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace skype_links_to_qr
{
    /// <summary>
    /// Interaction logic for qr.xaml
    /// </summary>
    public partial class qr : Window
    {

        public qr()
        {
            InitializeComponent();
            SelfRef = this;        
        }

        /// <summary>
        /// Позволяет обращатся к форме из других классов
        /// </summary>
        public static qr SelfRef
        {
            get;
            set;
        }

        /// <summary>
        /// Метод запускает таймер
        /// </summary>
        /// <param name="interval">время показа каринки</param>
        public void TimerStart(int interval)
        {
            DispatcherTimer time;
            time = new System.Windows.Threading.DispatcherTimer();
            time.Tick += new EventHandler(delegate { this.Close(); });
            time.Interval = new TimeSpan(0, 0, 0, 0, interval);
            time.Start();
        }

        /// <summary>
        /// Метод задает размеры qr
        /// </summary>
        /// <param name="x">высота qr</param>
        /// <param name="y">ширина qr</param>
        public void SetWinSize(int x,int y)
        {
            this.qrImage.Height = x;
            this.qrImage.Width = y;
        }

        /// <summary>
        /// Двигаем форму мышкой
        /// </summary>
        private void qrImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try //костыль, чтобы не вылетала при нажатии правой кнопки
            {
                this.DragMove();
            }

            catch
            {
                // затычка
            }
        }


    }
}
