﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;

using MessagingToolkit.Barcode.QRCode.Decoder;
using MessagingToolkit.Barcode.Pdf417.Encoder;
using MessagingToolkit.Barcode.QRCode;
using MessagingToolkit.Barcode;

//using SKYPE4COMLib;

namespace skype_links_to_qr
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App :  Application
    {

        private MessagingToolkit.Barcode.BarcodeEncoder barcodeEncoder = new MessagingToolkit.Barcode.BarcodeEncoder();  //хранит qrкод
        private List<string> param; // храним параметры qrкода

        /// <summary>
        /// Конструктор, в котором создается иконка в трее, окон нет
        /// </summary>
        public App()
        {
            this.ShutdownMode = ShutdownMode.OnExplicitShutdown;  //при закрытии окна приложение не завершается
            SelfRef = this;

            QrParam qr = new QrParam("set.xml"); // тянем параметры qr из файла
            param = qr.ParamGet();

            var ni = new System.Windows.Forms.NotifyIcon(); // создаем иконку в трее
            ni.Visible = true;
            ni.Icon= new Icon(@"E:\work\c#\skype-link-to-qr-wpf\skype-links-to-qr\1352812237_barcode-2d.ico");

            /*ni.Click += (s, _) =>
                {
                    var uri = new Uri("settingsWindow.xaml", UriKind.Relative);
                    var wnd = Application.LoadComponent(uri) as Window;
                    wnd.Visibility = Visibility.Visible;
                    wnd.ShowInTaskbar = false;
                };*/

            // создаем контексное меню в трее и наделяем его действиями
            ni.ContextMenu = new System.Windows.Forms.ContextMenu(new[]  
            {
                new System.Windows.Forms.MenuItem("Settings",delegate {
                var uri = new Uri("settingsWindow.xaml", UriKind.Relative);
                var wnd = Application.LoadComponent(uri) as Window;
                wnd.Visibility = Visibility.Visible;
                wnd.ShowInTaskbar = false;
                }), new System.Windows.Forms.MenuItem("Exit",delegate
                    {
                        ni.Visible=false;
                        this.Shutdown();
                    })
            });
            GetStarted(); // зцепляемся к skype
        }

        /// <summary>
        /// Позволяет обращатся к форме из других классов
        /// </summary>
        public static App SelfRef
        {
            set;
            get;
        }

        /// <summary>
        /// Подключаемся к skype, создаем обработчик србытий прихида сообщений в skype
        /// </summary>
        private void GetStarted()
        {
            QrGenerate("trololo");
            /*SKYPE4COMLib.Skype skype = new SKYPE4COMLib.Skype();
            skype.Attach(6, false);
            skype.MessageStatus += new SKYPE4COMLib._ISkypeEvents_MessageStatusEventHandler(skype_MessageStatus);*/
        }


        /// <summary>
        /// Обработчик событий прихода сообщения в чат skype
        /// </summary>
        /// <param name="msg"> текст пришедшего сообщения</param>
        /// <param name="status">состояние сообщения(прочитано, непрочитано...)</param>
        private void skype_MessageStatus(SKYPE4COMLib.ChatMessage msg, SKYPE4COMLib.TChatMessageStatus status)
        {
            char[] delimeters = { ' ', '\r', '\n' };
            if (status.ToString() == "cmsRead" || status.ToString() == "cmsReceived")
            {
                string[] s = msg.Body.Split(delimeters); // ищем ссылки
                foreach (string str in s)
                {
                    if (str.Length > 5)
                        if (str.Contains("http"))
                            QrGenerate(str); // если нашли, то генерируем qr
                }
            }
        }


        /// <summary>
        /// Метод создает qr код
        /// </summary>
        /// <param name="s">строка для генерации qr</param>
        private void QrGenerate(string s)
        {
            barcodeEncoder.CharacterSet = "ISO-8859-1";
            barcodeEncoder.ErrorCorrectionLevel = ErrorCorrectionLevel.L;
            //barcodeEncoder.ForeColor = //btnQRCodeForeColor.BackColor;
            //barcodeEncoder.BackColor = btnQRCodeBackColor.BackColor;
            barcodeEncoder.Margin = 0;//(int)numQRCodeQuietZone.Value;
            barcodeEncoder.Width = Convert.ToInt32(param[2]);//(int)numQRCodeWidth.Value;
            barcodeEncoder.Height = Convert.ToInt32(param[3]);//(int)numQRCodeHeight.Value;

            Dictionary<MessagingToolkit.Barcode.EncodeOptions, object> encodingOptions = new Dictionary<MessagingToolkit.Barcode.EncodeOptions, object>(1);
            qr q = new qr(); 
            q.SetWinSize(Convert.ToInt32(param[2]), Convert.ToInt32(param[3])); // задаем размеры картинки с кодом
            q.TimerStart(Convert.ToInt32(param[4])*1000); // задаем время показа картинки с кодом
            q.Show();

            qr.SelfRef.qrImage.Source = barcodeEncoder.Encode(MessagingToolkit.Barcode.BarcodeFormat.QRCode, s);
        }
    }
}
